# README #

## Reproduction of installation errors at foxx-manager

### Required:
docker (debian or ubuntu)

### Steps to reproduce:
- 1 clone repo
- 2 cange folder to d3X (X=1,2,3)
- 3 execute `1.docker3.X_up.sh`
- 4 wait about 1 minute
- 5 check logs with `2.docker3.X_logs.sh`
- 6 cleanup with `3.docker3.X_rm.sh`

### Folders to reproduce of installation errors:
- d31 for ArangoDB v3.1 (Docker 3.1)
- d32 for ArangoDB v3.2 (Docker 3.2)
- d33 for ArangoDB v3.2 (Docker 3.3)

### Execute scripts:
- `init.js` - install a service with `aragosh`
- `init.sh` - install a service with `foxx-manager`

### Results:
- ArangoDB v3.1 works 
- ArangoDB v3.2 get error `ArangoError 10: invalid URL` (both scripts)
- ArangoDB v3.3 get error `ArangoError 10: invalid URL` (both scripts)

