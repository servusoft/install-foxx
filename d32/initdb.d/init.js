#!/usr/bin/arangosh --javascript.execute

const internal = require("internal");

const foxx = require("@arangodb/foxx/manager")
internal.print(foxx)

const app1='hello-foxx';
const app2='git:Lean5/arango-prometheus-exporter:master';

 try {
  internal.print('install app1');
  foxx.install(app1, '/app1');
  internal.print('app1 Ok');
 } catch (e) {
  internal.print(e)
}

try {
  internal.print('install app2');
  foxx.install(app2, '/app2');
  internal.print('app2 Ok');
 } catch (e) {
  internal.print(e)
}

lst=foxx.list();
internal.print(lst)
