#!/bin/sh
docker run \
  --name arangodb-3.2 \
  -e ARANGO_ROOT_PASSWORD=root \
  -v `pwd`/initdb.d:/docker-entrypoint-initdb.d \
  -v `pwd`/arangodb3:/var/lib/arangodb3 \
  -v `pwd`/arangodb3-apps:/var/lib/arangodb3-apps \
  -p 8529:8529 -d \
  arangodb:3.2
